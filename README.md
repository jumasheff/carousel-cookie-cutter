# Carousel images REST API

## Local development
You need docker and docker-compose to be able to run the project
### Build docker containers
```bash
docker-compose -f local.yml build
```
### Load DB fixtures
```bash
docker-compose -f local.yml run --rm django python manage.py loaddata carousel/images_app/fixtures/testdata.json
```
### Move image fixtures to media dir
```bash
docker-compose -f local.yml run --rm django python manage.py move_fixtures_to_media
```
### Run containers
```bash
docker-compose -f local.yml up -d
```
### ... and checkout:
```
http://localhost:8000/v1/carousels/1/
```
### Run tests
```bash
docker-compose -f local.yml run --rm django python manage.py test
```

## Deployment
- At the moment, the project is set up to be served under https://carousel.ml address. If you want to use the code with your own domain, find and replace carousel.ml with your value. 
- Add your ssh key to production server. If you want to play around with my personal digitalocean droplet, ping me via telegram or email me: jumasheff@gmail.com, I'll give you secrets and add your ssh key to authorized_keys for you to be able to push your code there without entering login/password.
- Run deployment script: deploy.sh
The project is accessible here: https://carousel.ml/v1/carousels/1/
- If you are deploying to another server, you'll have to add .env files to the project before running deployment script:
  - Navigate to .envs
  - Add .production dir
  - cd to .production
  - Add .caddy file with the contents, specified in example_configs/example_config_caddy.txt
  - Add .django file with the contents, specified in example_configs/example_config_django.txt
  - Add .postgres file with the contents, specified in example_configs/example_config_postgres.txt
  - Push changes to your repo
  - Adjust deploy.sh
  - Add your id_rsa.pub contents to ~/.ssh/authorized_keys of your prod server
  - Run deploy.sh
  - When run for the first time, caddy will attempt to get SSL-certificate from Let's Encrypt.


## Credits: This project is built with Daniel Roy Greenfeld's (aka pydanny) cookiecutter. Nuff respect.
url: https://github.com/pydanny/cookiecutter-django/
