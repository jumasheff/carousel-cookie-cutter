from django.shortcuts import render
from rest_framework import viewsets
from .models import Carousel, Image
from .serializers import CarouselSerializer


class CarouselViewSet(viewsets.ModelViewSet):
    queryset = Carousel.objects.all()
    serializer_class = CarouselSerializer
    http_method_names = ['get']
