from .models import Carousel, Image
from rest_framework import serializers


class CarouselSerializer(serializers.ModelSerializer):
    images = serializers.StringRelatedField(many=True)

    class Meta:
        model = Carousel
        fields = ['id', 'name', 'images']
