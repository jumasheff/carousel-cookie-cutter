from django.test import TestCase
from rest_framework import status
from rest_framework.test import APITestCase
from django.core.management import call_command


class CarouselTest(APITestCase):

    def setUp(self):
        call_command('loaddata', 'carousel/images_app/fixtures/testdata.json')
        self.resource_url = '/v1/carousels/'
        self.individual_resource_url = self.resource_url + '1/'

    def test_root_resource_returns_list_of_carousels(self):
        response = self.client.get(self.resource_url)
        data = [
            {
                "id": 1,
                "name": "first",
                "images": [
                    "/media/images/2018/04/29/CodeBlocks.png",
                    "/media/images/2018/04/29/freenom.png",
                    "/media/images/2018/04/29/SublConsole3.png"
                ]
            },
            {
                "id": 2,
                "name": "second",
                "images": [
                    "/media/images/2018/04/29/jardam.png",
                    "/media/images/2018/04/29/paymentIsPossible.png",
                    "/media/images/2018/04/29/car_snow.jpg",
                    "/media/images/2018/04/29/ssh.png"
                ]
            }
        ]
        self.assertEqual(response.data, data)

    def test_individual_carousel_resource_returns_images_list(self):
        response = self.client.get(self.individual_resource_url)
        data = {
            "id": 1,
            "name": "first",
            "images": [
                "/media/images/2018/04/29/CodeBlocks.png",
                "/media/images/2018/04/29/freenom.png",
                "/media/images/2018/04/29/SublConsole3.png"
            ]
        }
        self.assertEqual(response.data, data)
