import shutil
import tempfile
from django.core.management import call_command
from django.test import override_settings
from test_plus.test import TestCase
from ..factories import CarouselFactory
from ..models import Image

class TestCarousel(TestCase):

    def setUp(self):
        self.carousel = CarouselFactory()

    def test__str__(self):
        self.assertEqual(
            self.carousel.__str__(),
            "carousel-0",
        )


class TestImage(TestCase):

    def setUp(self):
        call_command('loaddata', 'carousel/images_app/fixtures/testdata.json')
        self.image = Image.objects.all().first()

    def test__str__(self):
        self.assertEqual(
            self.image.__str__(),
            "/media/images/2018/04/29/CodeBlocks.png"
        )
