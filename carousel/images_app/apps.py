from django.apps import AppConfig


class ImagesAppConfig(AppConfig):
    name = 'carousel.images_app'
    verbose_name = "Images app"
