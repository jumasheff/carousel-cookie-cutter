from django.db import models


class Carousel(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Image(models.Model):
    image_file = models.ImageField(upload_to='images/%Y/%m/%d')
    carousel = models.ForeignKey(
        Carousel,
        related_name='images',
        related_query_name='image',
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.image_file.url

