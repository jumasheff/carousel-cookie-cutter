import shutil
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):
    help = 'Moves images folder with its files to media folder'

    def handle(self, *args, **options):
        dir_to_copy = str(settings.APPS_DIR) + '/images_app/fixtures/images/'
        destination = str(settings.MEDIA_ROOT) + '/images/'
        try:
            shutil.copytree(dir_to_copy, destination)
        except OSError:
            pass
