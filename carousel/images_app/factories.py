import factory


class CarouselFactory(factory.django.DjangoModelFactory):
    name = factory.Sequence(lambda n: f"carousel-{n}")

    class Meta:
        model = "images_app.Carousel"
        django_get_or_create = ("name",)

