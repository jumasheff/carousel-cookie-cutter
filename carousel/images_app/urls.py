from django.urls import path
from django.conf.urls import include
from rest_framework import routers
from . import views


app_name = 'images_app'

router = routers.DefaultRouter()
router.register(r'carousels', views.CarouselViewSet)


urlpatterns = [
    path('', include(router.urls)),
]
