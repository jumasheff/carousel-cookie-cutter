#!/bin/bash
set -e

ssh -T murat@carousel.ml << END
   cd /home/murat/carousel/
   echo 'Changed direction'
   git pull git@gitlab.com:jumasheff/carousel-cookie-cutter.git

   echo 'git pull'
   docker-compose -f production.yml build
   docker-compose -f production.yml up -d
END
